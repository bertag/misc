#Java Miscellaneous

This library is a collection of small one-off java classes that are not reasonably formed into their own repository. This library has no dependencies other than Java SE. For other more complicated libraries consider creating a separate repository.

## BatchExecutorService

A type of executor service that processes collections (batches) of items in separate threads. Processing items in a batch can be much more efficient than processing each item individually. As items are submitted to this executor, the items are placed in a batch which is then given to a separate thread to be processed. Having separate threads process the batch decouples the submission and the actual execution. The executor returns a `Future` so the item can be tracked through the process and an optional result returned.

```java
BatchRunnable<Integer, Double> r = b -> b.stream().map(e -> Math.pow(e, 2)).collect(Collectors.toList());
BatchExecutorService<Integer, Double> executor = new BatchExecutorService.Builder(r).build();

Future<Double> f1 = executor.submit(1);
Future<Double> f2 = executor.submit(2);
Future<Double> f3 = executor.submit(3);

System.out.println(f1.get());
System.out.println(f2.get());
System.out.println(f3.get());

executor.shutdownAndWait();
```

## PrimaryMonitor

A monitor that can be used to determine which instance of an application in a multi-instance deployment should perform background processing that is inherently single threaded.

```java
public class Harvester {

  private PrimaryMonitor monitor;

  public void init(){
    PrimaryMonitorDatabase db = new MongoPrimaryMonitorDatabase();
    monitor = new PrimaryMonitor.Builder(db).build();
  }

  public void shutdown(){
    monitor.shutdown();
  }

  public void harvest(){
    if(!harvest.isPrimary()){
      return;
    }
    ...
  }
}
```

## Common Helper Functions

A number of common functions for **Strings**, **Lists**, **Optionals**, **Resources**, and **Streams** are available as static helper methods inside the five above-named classes.  Among included functions are null-safe and empty-safe methods for returning the first or last element of a List, null-safe methods for asserting that a given String is non-blank (having at least one non-whitespace character), or methods for creating non-parallel Streams from Iterables or Iterators.

Future versions of Java may support some of these operations natively; as this happens, the convenience methods provided here will be deprecated to encourage use of the standard tools.
