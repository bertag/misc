package edu.byu.hbll.misc;

/**
 * A database interface for the {@link PrimaryMonitor}. Classes implementing this interface must
 * look in the same place across multiple instances of the application to read and write a string.
 * For example, a MongoPrimaryMonitorDatabase could look in a particular document for the value, a
 * SqlPrimaryMonitorDatabase could look in a particular table for the value, etc. The implementation
 * for testing even stores the value in a simple String.
 *
 * @author bwelker
 */
public interface PrimaryMonitorDatabase {

  /**
   * Read the value.
   *
   * @return the value.
   */
  String readValue() throws Exception;

  /**
   * Write the value.
   *
   * @param value - the value.
   */
  void writeValue(String value) throws Exception;
}
