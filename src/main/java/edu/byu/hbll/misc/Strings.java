package edu.byu.hbll.misc;

import java.util.Optional;
import java.util.function.Supplier;

/**
 * This class consists exclusively of static convenience or utility methods related to common string
 * operations.
 */
public class Strings {

  /**
   * Transforms the provided string such that if it is null or contains only whitespace characters,
   * an empty {@link Optional} will be returned, otherwise a non-empty Optional containing the
   * trimmed form of the string will be returned.
   *
   * @param str the string to transform
   * @return the resulting string as described
   */
  public static Optional<String> trimToOptional(String str) {
    return Optional.ofNullable(str).map(String::trim).filter(s -> !s.isEmpty());
  }

  /**
   * Returns {@code true} if the string is null, empty, or contains only whitespace; otherwise,
   * returns {@code false}.
   *
   * @return whether the string is blank or not
   */
  public static boolean isBlank(String str) {
    return !trimToOptional(str).isPresent();
  }

  /**
   * Verifies that the given string is non-null and contains at least one non-whitespace character.
   *
   * @param str the string to validate
   * @return the given string
   * @throws IllegalArgumentException if the given string is {@code null}, empty, or blank
   *     (containing only whitespace characters)
   */
  public static String requireNonBlank(String str) {
    if (isBlank(str)) {
      throw new IllegalArgumentException();
    }
    return str;
  }

  /**
   * Verifies that the given string is non-null and contains at least one non-whitespace character.
   *
   * @param str the string to validate
   * @return the given string
   * @throws IllegalArgumentException if the given string is {@code null}, empty, or blank
   *     (containing only whitespace characters)
   */
  public static String requireNonBlank(String str, String message) {
    if (isBlank(str)) {
      throw new IllegalArgumentException(message);
    }
    return str;
  }

  /**
   * Verifies that the given string is non-null and contains at least one non-whitespace character.
   *
   * @param str the string to validate
   * @return the given string
   * @throws IllegalArgumentException if the given string is {@code null}, empty, or blank
   *     (containing only whitespace characters)
   */
  public static String requireNonBlank(String str, Supplier<String> messageSupplier) {
    if (isBlank(str)) {
      throw new IllegalArgumentException(messageSupplier.get());
    }
    return str;
  }
}
