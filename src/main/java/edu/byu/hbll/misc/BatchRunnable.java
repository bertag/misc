package edu.byu.hbll.misc;

import java.util.List;

/**
 * An object that does processing on a collection (batch) of items. The nature of the processing is
 * defined by the implementation.
 *
 * @param <E> the batch element type
 * @param <V> the result type
 */
@FunctionalInterface
public interface BatchRunnable<E, V> {

  /**
   * Processes a batch of items and returns an optional list of results. This method is thread safe.
   *
   * @param batch the batch of items.
   * @return a list of results from the process. The list should contain one result for each item in
   *     the batch and be returned in the same iterable order as the batch. If there are no results
   *     from the process, null or an empty list may be returned instead.
   */
  List<V> run(List<E> batch);
}
