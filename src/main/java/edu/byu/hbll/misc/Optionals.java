package edu.byu.hbll.misc;

import java.util.Optional;
import java.util.function.Supplier;

/**
 * This class consists exclusively of static convenience or utility methods related to {@link
 * Optional}.
 */
public class Optionals {

  /**
   * If a value is present in the given {@link Optional}, returns an {@link Optional} describing the
   * value; otherwise returns an {@link Optional} produced by the supplier function.
   *
   * @deprecated use {@code Optional<T>.or(Supplier<T> supplier)} method in Java 9+.
   * @param <T> the type of optional
   * @param optional the preferred value
   * @param supplier the supplier of an alternate value if the preferred value is not present
   * @return an optional as described
   */
  @Deprecated
  public static <T> Optional<T> or(Optional<T> optional, Supplier<Optional<T>> supplier) {
    return optional.isPresent() ? optional : supplier.get();
  }

  /**
   * Constructs a new instance; since this is a utility class, this constructor will never be used.
   */
  private Optionals() {
    // Do nothing.
  }
}
