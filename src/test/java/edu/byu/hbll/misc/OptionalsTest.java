package edu.byu.hbll.misc;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Optional;
import java.util.function.Supplier;
import org.junit.jupiter.api.Test;

/** Unit tests for {@link Optionals}. */
public class OptionalsTest {

  private static final Optional<String> FIRST = Optional.of("a");
  private static final Optional<String> SECOND = Optional.of("b");
  private static final Optional<String> EMPTY = Optional.empty();

  /**
   * Verifies that calling {@link Optionals#or(Optional, Supplier)} will return the first argument
   * if it is present.
   */
  @Test
  public void orShouldReturnFirstOptionalIfPresent() {
    assertEquals(FIRST, Optionals.or(FIRST, () -> SECOND));
  }

  /**
   * Verifies that calling {@link Optionals#or(Optional, Supplier)} will return the second argument
   * if the first argument is not present.
   */
  @Test
  public void orShouldReturnSecondOptionalIfFirstIsEmpty() {
    assertEquals(SECOND, Optionals.or(EMPTY, () -> SECOND));
  }
}
